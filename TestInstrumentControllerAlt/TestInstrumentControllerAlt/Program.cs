﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TestInstrumentControllerAlt
{
    public class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            switch (random.Next(3))
            {
                case 0:
                    RunDefaultSupply();
                    break;
                case 1:
                    RunVendorASupply();
                    break;
                case 2:
                    RunVendorBSupply();
                    break;
            }    

            Console.ReadKey();
        }

        static void doTest()
        {
            Console.WriteLine("Running Tests...");
            Thread.Sleep(300);
        }

        static void RunDefaultSupply()
        {
            PowerSupply supply = new PowerSupply();

            Console.WriteLine("Using: " + supply.ToString());
            Console.ReadKey();
            supply.SetCurrent(0.1);
            supply.SetVoltage(5.0);
            supply.On();

            string config = supply.GetConfig();

            Console.WriteLine(config);

            if (!config.Contains("off"))
            {
                doTest(); 
            }else{
                Console.WriteLine("Invalid supply setup. Exiting Test.");
                return;
            }
            
            supply.Off();
            Console.WriteLine(supply.GetConfig());
        }

        static void RunVendorASupply()
        {
            VendorASupply supply = new VendorASupply();

            Console.WriteLine("Using: " + supply.ToString());
            Console.ReadKey();
            supply.SetCurrentLimit(0.1);
            supply.Apply(5.0);

            var config = supply.ReadState();
            if (config.Item3)
                Console.WriteLine(String.Format("The supply is outputting {0} volts", config.Item1));
            else
                Console.WriteLine("The supply is off");

            if (config.Item3)
            {
                doTest();
            }
            else
            {
                Console.WriteLine("Invalid supply setup. Exiting Test.");
                return;
            }

            supply.Reset();

            config = supply.ReadState();
            if (config.Item3)
                Console.WriteLine(String.Format("The supply is outputting {0} volts", config.Item1));
            else
                Console.WriteLine("The supply is off");
        }

        static void RunVendorBSupply()
        {
            VendorBSupply supply = new VendorBSupply();

            Console.WriteLine("Using: " + supply.ToString());
            Console.ReadKey();
            supply.SetVoltage(5.0);
            supply.On();

            if (supply.IsActive())
                Console.WriteLine(String.Format("The supply is outputting {0} volts", supply.GetVoltage()));
            else
                Console.WriteLine("The supply is off");

            if (supply.IsActive())
            {
                doTest();
            }
            else
            {
                Console.WriteLine("Invalid supply setup. Exiting Test.");
                return;
            }

            supply.Off();
            if (supply.IsActive())
                Console.WriteLine(String.Format("The supply is outputting {0} volts", supply.GetVoltage()));
            else
                Console.WriteLine("The supply is off");
        }
    }
}
