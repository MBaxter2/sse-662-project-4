﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInstrumentControllerAlt
{
    public class VendorASupply
    {
        private double voltage;
        private double currentLimit;
        private bool isOn;

        public void SetCurrentLimit(double current)
        {
            this.currentLimit = current;
        }

        public void Apply(double voltage)
        {
            this.voltage = voltage;
            this.isOn = true;
        }

        public void Reset()
        {
            this.voltage = 0;
            this.currentLimit = 0;
            this.isOn = false;
        }

        public Tuple<double, double, bool> ReadState()
        {
            return new Tuple<double, double, bool>(voltage, currentLimit, isOn);
        }

        public override string ToString()
        {
            return "Vendor A Supply";
        }
    }
}
