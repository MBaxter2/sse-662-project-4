﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInstrumentController
{
    public class VendorBSupplyAdapter : IPowerSupply
    {
        VendorBSupply supply;

        public VendorBSupplyAdapter(VendorBSupply supply)
        {
            this.supply = supply;
        }

        public void SetVoltage(double voltage)
        {
            supply.SetVoltage(voltage);
        }

        public void SetCurrent(double current)
        {
            return; // Do nothing
        }

        public void On()
        {
            supply.On();
        }

        public void Off()
        {
            supply.Off();
        }

        public string GetConfig()
        {
            if (supply.IsActive())
                return String.Format("The supply is outputting {0} volts", supply.GetVoltage());
            else
                return "The supply is off";
        }

        public override string ToString()
        {
            return "Vendor B Supply";
        }
    }
}
