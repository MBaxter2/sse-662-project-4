﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInstrumentController
{
    public class VendorBSupply
    {
        private double voltage;
        private bool on;

        public VendorBSupply()
        {
            voltage = 0;
            on = false;
        }

        public void SetVoltage(double voltage)
        {
            this.voltage = voltage;
        }

        public void On()
        {
            on = true;
        }

        public void Off()
        {
            on = false;
        }

        public double GetVoltage()
        {
            return voltage;
        }

        public bool IsActive()
        {
            return on;
        }
    }
}
