﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInstrumentController
{
    public class VendorASupplyAdapter : IPowerSupply
    {
        VendorASupply supply;
        double voltage;

        public VendorASupplyAdapter(VendorASupply supply)
        {
            this.supply = supply;
            voltage = 0;
        }

        public void SetVoltage(double voltage)
        {
            this.voltage = voltage;
        }

        public void SetCurrent(double current)
        {
            supply.SetCurrentLimit(current);
        }

        public void On()
        {
            supply.Apply(voltage);
        }

        public void Off()
        {
            supply.Reset();
        }

        public string GetConfig()
        {
            var config = supply.ReadState();
            if (config.Item3)
                return String.Format("The supply is outputting {0} volts", config.Item1);
            else
                return "The supply is off";
        }

        public override string ToString()
        {
            return "Vendor A Supply";
        }
    }
}
