﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInstrumentController
{
    public class PowerSupply : IPowerSupply
    {
        private double voltage;
        private double current;
        private bool state;

        public PowerSupply()
        {
            voltage = 0;
            current = 0;
            state = false;
        }

        public void SetVoltage(double voltage)
        {
            this.voltage = voltage;
        }

        public void SetCurrent(double current)
        {
            this.current = current;
        }

        public void On()
        {
            state = true;
        }

        public void Off()
        {
            state = false;
        }

        public string GetConfig()
        {
            if(state){
                return String.Format("The supply is outputting {0} volts", voltage);
            }else{
                return "The supply is off";
            }
        }

        public override string ToString()
        {
            return "Default Supply";
        }
    }
}
