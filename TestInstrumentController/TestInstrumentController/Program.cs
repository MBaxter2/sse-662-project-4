﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TestInstrumentController
{
    class Program
    {
        static void Main(string[] args)
        {
            IPowerSupply supply = getSupply();

            Console.WriteLine("Using: " + supply.ToString());
            Console.ReadKey();
            supply.SetCurrent(0.1);
            supply.SetVoltage(5.0);
            supply.On();

            string config = supply.GetConfig();

            Console.WriteLine(config);

            if (!config.Contains("off"))
            {
                doTest(); 
            }else{
                Console.WriteLine("Invalid supply setup. Exiting Test.");
                return;
            }
            
            supply.Off();
            Console.WriteLine(supply.GetConfig());
            Console.ReadKey();
        }

        static void doTest()
        {
            Console.WriteLine("Running Tests...");
            Thread.Sleep(300);
        }

        static IPowerSupply getSupply()
        {
            Random random = new Random();
            int i = random.Next(3);

            switch (i)
            {
                case 0:
                    return new PowerSupply();
                case 1:
                    return new VendorASupplyAdapter(new VendorASupply());
                case 2:
                    return new VendorBSupplyAdapter(new VendorBSupply());
            }            

            return new PowerSupply();
        }
    }
}
