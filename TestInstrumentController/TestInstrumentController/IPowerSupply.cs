﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestInstrumentController
{
    public interface IPowerSupply
    {
        void SetVoltage(double voltage);
        void SetCurrent(double current);
        void On();
        void Off();
        string GetConfig();
    }
}
