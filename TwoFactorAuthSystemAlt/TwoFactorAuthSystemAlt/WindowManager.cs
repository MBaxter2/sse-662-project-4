﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;

namespace TwoFactorAuthSystemAlt
{
    public class WindowManager : INotifyPropertyChanged
    {
        private Command insertCard;
        public Command InsertCard { get { return insertCard; } }

        private Command removeCard;
        public Command RemoveCard { get { return removeCard; } }

        private Command checkPassword;
        public Command CheckPassword { get { return checkPassword; } }

        private int failedAttempts;
        public int FailedAttempts
        {
            get { return failedAttempts; }
            set 
            { 
                failedAttempts = value; 
                OnPropertyChanged("FailedAttempts");
            }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                OnPropertyChanged("Password");
            }
        }

        public string state;
        public string State
        {
            get { return state; }
            set
            {
                state = value;
                OnPropertyChanged("State");
            }
        }

        private LoginManager manager;

        public WindowManager()
        {
            manager = new LoginManager();
            insertCard = new Command(doInsertCard);
            removeCard = new Command(doRemoveCard);
            checkPassword = new Command(doCheckPassword);
            State = manager.ToString();
        }
    
        private void doInsertCard()
        {
            try
            {
                manager.InsertCard();
            }
            catch(StateActionException e)
            {
                MessageBox.Show(e.Message);
            }
            FailedAttempts = manager.FailedAttempts;
            State = manager.ToString();
        }

        private void doRemoveCard()
        {
            try
            {
                manager.RemoveCard();
            }
            catch (StateActionException e)
            {
                MessageBox.Show(e.Message);
            }
            FailedAttempts = manager.FailedAttempts;
            State = manager.ToString();
        }

        private void doCheckPassword()
        {
            try
            {
                manager.CheckPassword(password);
            }
            catch (StateActionException e)
            {
                MessageBox.Show(e.Message);
            }
            FailedAttempts = manager.FailedAttempts;
            State = manager.CurrentState.ToString();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
        private void OnPropertyChanged(string property)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(property));
        }
    }
}
