﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoFactorAuthSystem
{
    public interface ILoginState
    {
        void InsertCard();
        void RemoveCard();
        void CorrectPassword();
        void IncorrectPassword();
    }
}
