﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoFactorAuthSystem
{
    public class LoggedInState : ILoginState
    {
        private readonly LoginManager manager;

        public LoggedInState(LoginManager manager)
        {
            this.manager = manager;
        }

        void ILoginState.InsertCard()
        {
            throw new StateActionException("A card has already been inserted");
        }

        void ILoginState.RemoveCard()
        {
            manager.CurrentState = manager.LoggedOut;
        }

        void ILoginState.CorrectPassword()
        {
            throw new StateActionException("Already logged in");
        }

        void ILoginState.IncorrectPassword()
        {
            throw new StateActionException("Already logged in");
        }

        public override string ToString()
        {
            return "Logged In";
        }
    }
}
