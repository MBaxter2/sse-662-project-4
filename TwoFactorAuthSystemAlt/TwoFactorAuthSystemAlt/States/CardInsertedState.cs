﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoFactorAuthSystem
{
    class CardInsertedState : ILoginState
    {
        private readonly LoginManager manager;

        public CardInsertedState(LoginManager manager)
        {
            this.manager = manager;
        }
        
        public void InsertCard()
        {
            throw new StateActionException("A card has already been inserted");
        }

        public void RemoveCard()
        {
            manager.CurrentState = manager.LoggedOut;
        }

        public void CorrectPassword()
        {
            manager.FailedAttempts = 0;
            manager.CurrentState = manager.LoggedIn;
        }

        public void IncorrectPassword()
        {
            manager.FailedAttempts += 1;
            if(manager.FailedAttempts > 3)
            {
                manager.CurrentState = manager.LockDown;
            }
            else
            {
                throw new StateActionException("Incorrect password entered");
            }
        }

        public override string ToString()
        {
            return "Card Inserted";
        }
    }
}
