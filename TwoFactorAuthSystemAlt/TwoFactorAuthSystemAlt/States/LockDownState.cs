﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoFactorAuthSystem
{
    public class LockDownState : ILoginState
    {
        private const string LOCKDOWN_ERROR = "This terminal is locked down, please see admin to restore functionality";
        private readonly LoginManager manager;

        public LockDownState(LoginManager manager)
        {
            this.manager = manager;
        }

        public void InsertCard()
        {
            throw new StateActionException(LOCKDOWN_ERROR);
        }

        public void RemoveCard()
        {
            throw new StateActionException(LOCKDOWN_ERROR);
        }

        public void CorrectPassword()
        {
            throw new StateActionException(LOCKDOWN_ERROR);
        }

        public void IncorrectPassword()
        {
            throw new StateActionException(LOCKDOWN_ERROR);
        }

        public override string ToString()
        {
            return "Lock Down";
        }
    }
}
