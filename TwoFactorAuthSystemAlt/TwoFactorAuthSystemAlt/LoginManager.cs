﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoFactorAuthSystemAlt
{
    public enum LoginState
    {
        CardInserted = 0,
        LoggedIn = 1,
        LoggedOut = 2,
        LockDown = 3
    };

    public class LoginManager
    {
        private const string LOCKDOWN_ERROR = "This terminal is locked down, please see admin to restore functionality";
        private const string NOCARD_ERROR = "There is no card inserted";

        private LoginState currentState;
        public LoginState CurrentState
        {
            get { return currentState; }
            set { currentState = value; }
        }

        private int failedAttempts;
        public int FailedAttempts
        {
            get { return failedAttempts; }
            set { failedAttempts = value; }
        }

        public LoginManager()
        {
            currentState = LoginState.LoggedOut;
        }

        public void InsertCard()
        {
            switch(currentState)
            {
                case LoginState.LoggedOut:
                    CurrentState = LoginState.CardInserted;
                    break;
                case LoginState.LoggedIn:
                    throw new StateActionException("A card has already been inserted");
                case LoginState.CardInserted:
                    throw new StateActionException("A card has already been inserted");
                case LoginState.LockDown:
                    throw new StateActionException(LOCKDOWN_ERROR);
            }
        }

        public void RemoveCard()
        {
            switch (currentState)
            {
                case LoginState.LoggedOut:
                    throw new StateActionException(NOCARD_ERROR);
                case LoginState.LoggedIn:
                    CurrentState = LoginState.LoggedOut;
                    break;
                case LoginState.CardInserted:
                    CurrentState = LoginState.LoggedOut;
                    break;
                case LoginState.LockDown:
                    throw new StateActionException(LOCKDOWN_ERROR);
            }
        }

        public void CheckPassword(string password)
        {
            if (validatePassword(password))
                correctPassword();
            else
                incorrectPassword();
        }

        private void correctPassword()
        {
            switch(currentState)
            {
                case LoginState.LoggedOut:
                    throw new StateActionException(NOCARD_ERROR);
                case LoginState.LoggedIn:
                    throw new StateActionException("Already logged in");
                case LoginState.CardInserted:
                    FailedAttempts = 0;
                    CurrentState = LoginState.LoggedIn;
                    break;
                case LoginState.LockDown:
                    throw new StateActionException(LOCKDOWN_ERROR);
            }
        }

        private void incorrectPassword()
        {
            switch (currentState)
            {
                case LoginState.LoggedOut:
                    throw new StateActionException(NOCARD_ERROR);
                case LoginState.LoggedIn:
                    throw new StateActionException("Already logged in");
                case LoginState.CardInserted:
                    FailedAttempts += 1;
                    if (FailedAttempts > 3)
                    {
                        CurrentState = LoginState.LockDown;
                    }
                    else
                    {
                        throw new StateActionException("Incorrect password entered");
                    }
                    break;
                case LoginState.LockDown:
                    throw new StateActionException(LOCKDOWN_ERROR);
            }
        }

        private bool validatePassword(string password)
        {
            return (password == "123badpassword");
        }

        public override string ToString()
        {
            switch (currentState)
            {
                case LoginState.LoggedOut:
                    return "Logged Out";
                case LoginState.LoggedIn:
                    return "Logged In";
                case LoginState.CardInserted:
                    return "Card Inserted";
                case LoginState.LockDown:
                    return "Locked Down";
            }

            return "Unknown State";
        }
    }
}
