﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExample
{
    public class Leather : Armor
    {
        public override double GetDefense()
        {
            return 2;
        }
    }
}
