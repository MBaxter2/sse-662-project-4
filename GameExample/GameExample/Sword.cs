﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExample
{
    public class Sword : Weapon
    {
        public override double GetAttack()
        {
            return 10;
        }

        public override string ToString()
        {
            return "Sword";
        }
    }
}
