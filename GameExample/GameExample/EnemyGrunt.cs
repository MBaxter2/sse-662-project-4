﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExample
{
    public class EnemyGrunt : GameCharacter
    {
        private int number;

        public EnemyGrunt(double healthPoints, List<GameCharacter> characters)
            : base(healthPoints, characters)
        {
            this.number = 0;
        }

        public EnemyGrunt(int number, double healthPoints, List<GameCharacter> characters)
            : base(healthPoints, characters)
        {
            this.number = number;
        }

        public override double CalculateBaseDamage()
        {
            return Weapon.GetAttack();
        }

        public override double CalculateFinalDamage(double initial)
        {
            double damage = initial - Armor.GetDefense();

            if (damage > 0)
                return damage;
            else
                return 0;
        }

        public override List<GameCharacter> Swing()
        {
            List<GameCharacter> hit = new List<GameCharacter>();
            foreach (var character in Characters)
            {
                if (character == this)
                    continue;

                Point diff = new Point();

                diff.X = character.Position.X - Position.X;
                diff.Y = character.Position.Y - Position.Y;

                switch (Orientation)
                {
                    case Direction.North:
                        if (diff.X >= 0 && diff.X <= 1 && diff.Y == 0)
                            hit.Add(character);
                        break;
                    case Direction.South:
                        if (diff.X <= 0 && diff.X >= -1 && diff.Y == 0)
                            hit.Add(character);
                        break;
                    case Direction.East:
                        if (diff.Y >= 0 && diff.Y <= 1 && diff.X == 0)
                            hit.Add(character);
                        break;
                    case Direction.West:
                        if (diff.Y <= 0 && diff.Y >= -1 && diff.X == 0)
                            hit.Add(character);
                        break;
                }
            }
            return hit;
        }

        public override string GetName()
        {
            if (number > 0)
                return "Enemy Grunt " + number;
            else
                return "Enemy Grunt";
        }
    }
}
