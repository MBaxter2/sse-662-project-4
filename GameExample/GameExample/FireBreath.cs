﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExample
{
    public class FireBreath : Weapon
    {
        public override double GetAttack()
        {
            return 100;
        }

        public override string ToString()
        {
            return "Fire Breath";
        }
    }
}
