﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExample
{
    public class Dragon : GameCharacter
    {
        private int number;

        public Dragon(double healthPoints, List<GameCharacter> characters)
            : base(healthPoints, characters)
        {
            Weapon = new FireBreath();
            Armor = new Scales();
            number = 0;
        }

        public Dragon(int number, double healthPoints, List<GameCharacter> characters)
            : base(healthPoints, characters)
        {
            Weapon = new FireBreath();
            Armor = new Scales();
            this.number = number;
        }

        public override double CalculateBaseDamage()
        {
            return Weapon.GetAttack();
        }

        public override double CalculateFinalDamage(double initial)
        {
            double damage = initial - Armor.GetDefense();

            if (damage > 0)
                return damage;
            else
                return 0;
        }

        public override List<GameCharacter> Swing()
        {
            List<GameCharacter> hit = new List<GameCharacter>();
            foreach (var character in Characters)
            {
                if (character == this)
                    continue;

                Point diff = new Point();

                diff.X = character.Position.X - Position.X;
                diff.Y = character.Position.Y - Position.Y;
                double distance = Math.Sqrt(Math.Pow(diff.X, 2) + Math.Pow(diff.Y, 2));

                if (distance <= 5)
                    hit.Add(character);
            }
            return hit;
        }

        public override string GetName()
        {
            if (number > 0)
                return "Dragon " + number;
            else
                return "Dragon";
        }
    }
}
