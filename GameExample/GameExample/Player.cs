﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExample
{
    public class Player : GameCharacter
    {
        public Player(double healthPoints, List<GameCharacter> characters)
            : base(healthPoints, characters)
        {
        }

        public override double CalculateBaseDamage()
        {
            double damage = Weapon.GetAttack();

            if(Weapon is Sword)
            {
                damage = damage * 1.1;
            }

            return damage + 5;
        }

        public override double CalculateFinalDamage(double initial)
        {
            double defense = Armor.GetDefense();

            double damage = initial - 1;
            damage = damage - defense;

            if (damage > 0)
                return damage;
            else
                return 0;
        }

        public override List<GameCharacter> Swing()
        {
            List<GameCharacter> hit = new List<GameCharacter>();
            foreach (var character in Characters)
            {
                if (character == this)
                    continue;

                Point diff = new Point();

                diff.X = character.Position.X - Position.X;
                diff.Y = character.Position.Y - Position.Y;
                double distance = Math.Sqrt(Math.Pow(diff.X, 2) + Math.Pow(diff.Y, 2));

                switch(Orientation)
                {
                    case Direction.North:
                        if (diff.X >= 0 && distance <= 1)
                            hit.Add(character);
                        break;
                    case Direction.South:
                        if (diff.X <= 0 && distance <= 1)
                            hit.Add(character);
                        break;
                    case Direction.East:
                        if (diff.Y >= 0 && distance <= 1)
                            hit.Add(character);
                        break;
                    case Direction.West:
                        if (diff.Y <= 0 && distance <= 1)
                            hit.Add(character);
                        break;
                }
            }
            return hit;
        }

        public override string GetName()
        {
            return "Player";
        }
    }
}
