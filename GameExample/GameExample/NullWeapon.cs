﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExample
{
    public class NullWeapon : Weapon
    {
        public override double GetAttack()
        {
            return 0;
        }
    }
}
