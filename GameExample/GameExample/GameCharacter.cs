﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExample
{
    public struct Point
    {
        public double X;
        public double Y;
    }

    public enum Direction
    {
        North,
        South,
        East,
        West
    }

    public abstract class GameCharacter
    {
        private double healthPoints;
        public double HealthPoints
        {
            get { return healthPoints; }
            set { healthPoints = value; }
        }

        private Point position; 
        public Point Position
        {
            get { return position; }
            set { position = value; }
        }

        private Direction orientation;
        public Direction Orientation
        {
            get { return orientation; }
            set { orientation = value; }
        }

        private Weapon weapon;
        public Weapon Weapon
        {
            get { return weapon; }
            set { weapon = value; }
        }

        private Armor armor;
        public Armor Armor
        {
            get { return armor; }
            set { armor = value; }
        }

        private List<GameCharacter> characters;
        public List<GameCharacter> Characters
        {
            get { return characters; }
        }

        public abstract double CalculateBaseDamage();
        public abstract double CalculateFinalDamage(double initial);
        public abstract List<GameCharacter> Swing();
        public abstract string GetName();

        public GameCharacter(double healthPoints, List<GameCharacter> characters)
        {
            this.healthPoints = healthPoints;
            this.position = new Point() {X=0, Y=0};
            this.orientation = Direction.North;
            this.weapon = new NullWeapon();
            this.armor = new NullArmor();
            this.characters = characters;
            Characters.Add(this);
        }

        public void Attack()
        {
            double damage = CalculateBaseDamage();
            List<GameCharacter> targets = Swing();
            StringBuilder names = new StringBuilder();

            foreach(var target in targets)
            {
                if (names.Length > 0)
                    names.Append(", ");

                names.Append(target.GetName());
            }

            if (targets.Count == 0)
                Console.WriteLine(String.Format("{0} attacked with {1} and missed", GetName(), weapon.ToString()));
            else
                Console.WriteLine(String.Format("{0} attacked with {1} and did {2} damage to {3}", GetName(), weapon.ToString(), damage, names.ToString()));

            foreach (var target in targets)
                target.TakeDamage(damage);
        }

        public void TakeDamage(double amount)
        {
            double damage = CalculateFinalDamage(amount);

            if(damage > 0)
                healthPoints -= damage;

            if (healthPoints > 0)
                Console.WriteLine(String.Format("{0} has {1} HP left", GetName(), healthPoints));
            else
                Console.WriteLine(string.Format("{0} died", GetName()));
        }
    }
}
