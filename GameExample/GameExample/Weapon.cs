﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExample
{
    public abstract class Weapon
    {
        public abstract double GetAttack();
    }
}
