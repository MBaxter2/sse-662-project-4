﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExample
{
    public class Ax : Weapon
    {
        public override double GetAttack()
        {
            return 9;
        }

        public override string ToString()
        {
            return "Ax";
        }
    }
}
