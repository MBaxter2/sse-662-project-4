﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoFactorAuthSystem
{
    class StateActionException : Exception
    {
        public StateActionException(string message) : base(message) { }
    }
}
