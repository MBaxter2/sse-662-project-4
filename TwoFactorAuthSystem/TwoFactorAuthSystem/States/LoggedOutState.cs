﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoFactorAuthSystem
{
    public class LoggedOutState : ILoginState
    {
        private const string NOCARD_ERROR = "There is no card inserted";
        private readonly LoginManager manager;

        public LoggedOutState(LoginManager manager)
        {
            this.manager = manager;
        }

        public void InsertCard()
        {
            manager.CurrentState = manager.CardInserted;
        }

        public void RemoveCard()
        {
            throw new StateActionException(NOCARD_ERROR);
        }

        public void CorrectPassword()
        {
            throw new StateActionException(NOCARD_ERROR);
        }

        public void IncorrectPassword()
        {
            throw new StateActionException(NOCARD_ERROR);
        }

        public override string ToString()
        {
            return "Logged Out";
        }
    }
}
