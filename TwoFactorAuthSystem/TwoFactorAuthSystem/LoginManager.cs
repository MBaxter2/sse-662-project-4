﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoFactorAuthSystem
{
    public class LoginManager
    {
        private readonly ILoginState cardInserted;
        public ILoginState CardInserted
        {
            get { return cardInserted; }
        }

        private readonly ILoginState loggedIn;
        public ILoginState LoggedIn
        {
            get { return loggedIn; }
        }

        private readonly ILoginState loggedOut;
        public ILoginState LoggedOut
        {
            get { return loggedOut; }
        }

        private readonly ILoginState lockDown;
        public ILoginState LockDown
        {
            get { return lockDown; }
        }

        private ILoginState currentState;
        public ILoginState CurrentState
        {
            get { return currentState; }
            set { currentState = value; }
        }

        private int failedAttempts;
        public int FailedAttempts
        {
            get { return failedAttempts; }
            set { failedAttempts = value; }
        }

        public LoginManager()
        {
            cardInserted = new CardInsertedState(this);
            loggedIn = new LoggedInState(this);
            loggedOut = new LoggedOutState(this);
            lockDown = new LockDownState(this);
            currentState = loggedOut;
        }

        public void InsertCard()
        {
            currentState.InsertCard();
        }

        public void RemoveCard()
        {
            currentState.RemoveCard();
        }

        public void CheckPassword(string password)
        {
            if (validatePassword(password))
                currentState.CorrectPassword();
            else
                currentState.IncorrectPassword();
        }

        private bool validatePassword(string password)
        {
            return (password == "123badpassword");
        }
    }
}
