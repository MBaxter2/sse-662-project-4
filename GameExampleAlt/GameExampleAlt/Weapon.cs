﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExampleAlt
{
    public abstract class Weapon
    {
        public abstract double GetAttack();
    }
}
