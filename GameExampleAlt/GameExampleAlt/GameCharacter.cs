﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExampleAlt
{
    public struct Point
    {
        public double X;
        public double Y;
    }

    public enum Direction
    {
        North,
        South,
        East,
        West
    }

    public abstract class GameCharacter
    {
        private double healthPoints;
        public double HealthPoints
        {
            get { return healthPoints; }
            set { healthPoints = value; }
        }

        private Point position; 
        public Point Position
        {
            get { return position; }
            set { position = value; }
        }

        private Direction orientation;
        public Direction Orientation
        {
            get { return orientation; }
            set { orientation = value; }
        }
        
        public GameCharacter(double healthPoints)
        {
            this.healthPoints = healthPoints;
            this.position = new Point() {X=0, Y=0};
            this.orientation = Direction.North;
        }

        public abstract void Hit(double damage);
        public abstract string GetName();
    }
}
