﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExampleAlt
{
    public class NullWeapon : Weapon
    {
        public override double GetAttack()
        {
            return 0;
        }
    }
}
