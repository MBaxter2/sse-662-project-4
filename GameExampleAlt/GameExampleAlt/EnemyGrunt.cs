﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExampleAlt
{
    public class EnemyGrunt : GameCharacter
    {
        private int number;

        private Weapon weapon;
        public Weapon Weapon
        {
            get { return weapon; }
            set { weapon = value; }
        }

        private Armor armor;
        public Armor Armor
        {
            get { return armor; }
            set { armor = value; }
        }

        public EnemyGrunt(double healthPoints)
            : base(healthPoints)
        {
            this.number = 0;
            this.weapon = new NullWeapon();
            this.armor = new NullArmor();
        }

        public EnemyGrunt(int number, double healthPoints)
            : base(healthPoints)
        {
            this.number = number;
            this.weapon = new NullWeapon();
            this.armor = new NullArmor();
        }

        public void Attack(List<GameCharacter> characters)
        {
            double damage = weapon.GetAttack();
            List<GameCharacter> hits = new List<GameCharacter>();
            foreach (var character in characters)
            {
                if (character.Position.X == Position.X && character.Position.Y == Position.Y)
                    continue;

                Point diff = new Point();

                diff.X = character.Position.X - Position.X;
                diff.Y = character.Position.Y - Position.Y;

                switch (Orientation)
                {
                    case Direction.North:
                        if (diff.X >= 0 && diff.X <= 1 && diff.Y == 0)
                            hits.Add(character);
                        break;
                    case Direction.South:
                        if (diff.X <= 0 && diff.X >= -1 && diff.Y == 0)
                            hits.Add(character);
                        break;
                    case Direction.East:
                        if (diff.Y >= 0 && diff.Y <= 1 && diff.X == 0)
                            hits.Add(character);
                        break;
                    case Direction.West:
                        if (diff.Y <= 0 && diff.Y >= -1 && diff.X == 0)
                            hits.Add(character);
                        break;
                }
            }

            StringBuilder names = new StringBuilder();
            foreach (var hit in hits)
            {
                if (names.Length > 0)
                    names.Append(", ");

                names.Append(hit.GetName());
            }

            if (hits.Count == 0)
                Console.WriteLine(String.Format("{0} attacked with {1} and missed", GetName(), weapon.ToString()));
            else
                Console.WriteLine(String.Format("{0} attacked with {1} and did {2} damage to {3}", GetName(), weapon.ToString(), damage, names.ToString()));

            foreach (var hit in hits)
                hit.Hit(damage);
        }

        public override void Hit(double damage)
        {
            damage = damage - armor.GetDefense();
            if(damage > 0)
                HealthPoints -= damage;

            if (HealthPoints > 0)
                Console.WriteLine(String.Format("{0} has {1} HP left", GetName(), HealthPoints));
            else
                Console.WriteLine(string.Format("{0} died", GetName()));
        }

        public override string GetName()
        {
            if (number > 0)
                return "Enemy Grunt " + number;
            else
                return "Enemy Grunt";
        }
    }
}
