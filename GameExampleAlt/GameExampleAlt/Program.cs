﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExampleAlt
{
    class Program
    {
        static void Main(string[] args)
        {
            Scenario1();

            Console.WriteLine(Environment.NewLine + Environment.NewLine);
            Scenario2();

            Console.WriteLine(Environment.NewLine + Environment.NewLine);
            Scenario3();

            Console.ReadKey();
        }

        public static void Scenario1()
        {
            Console.WriteLine("Running Scenario 1");

            List<GameCharacter> characters = new List<GameCharacter>();

            Player player = new Player(100);
            EnemyGrunt grunt1 = new EnemyGrunt(1, 20);
            EnemyGrunt grunt2 = new EnemyGrunt(2, 20);
            EnemyGrunt grunt3 = new EnemyGrunt(3, 5);

            characters.Add(player);
            characters.Add(grunt1);
            characters.Add(grunt2);
            characters.Add(grunt3);

            player.Weapon = new Sword();
            grunt1.Weapon = new Sword();
            grunt2.Weapon = new Spear();
            grunt3.Weapon = new Ax();

            player.Armor = new Leather();
            grunt1.Armor = new Leather();
            grunt2.Armor = new Leather();
            grunt3.Armor = new Leather();

            player.Position = new Point() { X = 2, Y = 2 };
            grunt1.Position = new Point() { X = 3, Y = 2 };
            grunt2.Position = new Point() { X = 0, Y = 0 };
            grunt3.Position = new Point() { X = 2, Y = 1 };
            
            player.Attack(characters);
        }

        public static void Scenario2()
        {
            Console.WriteLine("Running Scenario 2");

            List<GameCharacter> characters = new List<GameCharacter>();

            Player player = new Player(100);
            EnemyGrunt grunt1 = new EnemyGrunt(1, 20);
            EnemyGrunt grunt2 = new EnemyGrunt(2, 20);
            EnemyGrunt grunt3 = new EnemyGrunt(3, 20);
            EnemyGrunt grunt4 = new EnemyGrunt(4, 20);

            characters.Add(player);
            characters.Add(grunt1);
            characters.Add(grunt2);
            characters.Add(grunt3);

            player.Weapon = new Sword();
            grunt1.Weapon = new Sword();
            grunt2.Weapon = new Spear();
            grunt3.Weapon = new Ax();
            grunt4.Weapon = new Spear();

            player.Armor = new ChainMail();
            grunt1.Armor = new Leather();
            grunt2.Armor = new Leather();
            grunt3.Armor = new Leather();

            player.Position = new Point() { X = 2, Y = 2 };
            grunt1.Position = new Point() { X = 3, Y = 2 };
            grunt2.Position = new Point() { X = 1, Y = 2 };
            grunt3.Position = new Point() { X = 2, Y = 3 };
            grunt4.Position = new Point() { X = 2, Y = 1 };

            grunt1.Orientation = Direction.South;
            grunt2.Orientation = Direction.South;
            grunt3.Orientation = Direction.West;
            grunt4.Orientation = Direction.East;

            grunt1.Attack(characters);
            grunt2.Attack(characters);
            grunt3.Attack(characters);
            grunt4.Attack(characters);
        }

        public static void Scenario3()
        {
            Console.WriteLine("Running Scenario 3");

            List<GameCharacter> characters = new List<GameCharacter>();

            Player player = new Player(100);
            EnemyGrunt grunt1 = new EnemyGrunt(1, 20);
            EnemyGrunt grunt2 = new EnemyGrunt(2, 20);
            EnemyGrunt grunt3 = new EnemyGrunt(3, 20);
            Dragon dragon = new Dragon(500);

            characters.Add(player);
            characters.Add(grunt1);
            characters.Add(grunt2);
            characters.Add(grunt3);
            characters.Add(dragon);

            player.Weapon = new Sword();
            grunt1.Weapon = new Sword();
            grunt2.Weapon = new Spear();
            grunt3.Weapon = new Ax();

            player.Armor = new ChainMail();
            grunt1.Armor = new Leather();
            grunt2.Armor = new Leather();
            grunt3.Armor = new Leather();

            dragon.Position = new Point() { X = 5, Y = 5 };
            player.Position = new Point() { X = 6, Y = 5 };
            grunt1.Position = new Point() { X = 5, Y = 6 };
            grunt2.Position = new Point() { X = 5, Y = 4 };
            grunt3.Position = new Point() { X = 9, Y = 1 };

            dragon.Orientation = Direction.North;
            player.Orientation = Direction.South;
            grunt1.Orientation = Direction.East;
            grunt2.Orientation = Direction.West;
            grunt3.Orientation = Direction.West;

            player.Attack(characters);
            dragon.BreatheFire(characters);
        }
    }
}
