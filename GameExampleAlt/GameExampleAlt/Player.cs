﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExampleAlt
{
    public class Player : GameCharacter
    {
        private Weapon weapon;
        public Weapon Weapon
        {
            get { return weapon; }
            set { weapon = value; }
        }

        private Armor armor;
        public Armor Armor
        {
            get { return armor; }
            set { armor = value; }
        }

        public Player(double healthPoints)
            : base(healthPoints)
        {
        }

        public void Attack(List<GameCharacter> characters)
        {
            double damage = Weapon.GetAttack();
            if (Weapon is Sword)
            {
                damage = damage * 1.1;
            }
            damage = damage + 5;

            List<GameCharacter> hits = new List<GameCharacter>();
            foreach (var character in characters)
            {
                if (character.Position.X == Position.X && character.Position.Y == Position.Y)
                    continue;

                Point diff = new Point();

                diff.X = character.Position.X - Position.X;
                diff.Y = character.Position.Y - Position.Y;
                double distance = Math.Sqrt(Math.Pow(diff.X, 2) + Math.Pow(diff.Y, 2));

                switch(Orientation)
                {
                    case Direction.North:
                        if (diff.X >= 0 && distance <= 1)
                            hits.Add(character);
                        break;
                    case Direction.South:
                        if (diff.X <= 0 && distance <= 1)
                            hits.Add(character);
                        break;
                    case Direction.East:
                        if (diff.Y >= 0 && distance <= 1)
                            hits.Add(character);
                        break;
                    case Direction.West:
                        if (diff.Y <= 0 && distance <= 1)
                            hits.Add(character);
                        break;
                }
            }

            StringBuilder names = new StringBuilder();
            foreach (var hit in hits)
            {
                if (names.Length > 0)
                    names.Append(", ");

                names.Append(hit.GetName());
            }

            if (hits.Count == 0)
                Console.WriteLine(String.Format("{0} attacked with {1} and missed", GetName(), weapon.ToString()));
            else
                Console.WriteLine(String.Format("{0} attacked with {1} and did {2} damage to {3}", GetName(), weapon.ToString(), damage, names.ToString()));

            foreach (var hit in hits)
                hit.Hit(damage);
        }

        public override void Hit(double damage)
        {
            double defense = Armor.GetDefense();

            damage = damage - 1;
            damage = damage - Armor.GetDefense();
            if(damage > 0)
                HealthPoints -= damage;

            if (HealthPoints > 0)
                Console.WriteLine(String.Format("{0} has {1} HP left", GetName(), HealthPoints));
            else
                Console.WriteLine(string.Format("{0} died", GetName()));
        }

        public override string GetName()
        {
            return "Player";
        }
    }
}
