﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExampleAlt
{
    public class Dragon : GameCharacter
    {
        private int number;

        public Dragon(double healthPoints)
            : base(healthPoints)
        {
            number = 0;
        }

        public Dragon(int number, double healthPoints)
            : base(healthPoints)
        {
            this.number = number;
        }

        public void BreatheFire(List<GameCharacter> characters)
        {
            double damage = 100;
            List<GameCharacter> hits = new List<GameCharacter>();
            foreach (var character in characters)
            {
                if (character.Position.X == Position.X && character.Position.Y == Position.Y)
                    continue;

                Point diff = new Point();

                diff.X = character.Position.X - Position.X;
                diff.Y = character.Position.Y - Position.Y;
                double distance = Math.Sqrt(Math.Pow(diff.X, 2) + Math.Pow(diff.Y, 2));

                if (distance <= 5)
                    hits.Add(character);
                else
                    hits.Add(character);
            }

            StringBuilder names = new StringBuilder();
            foreach (var hit in hits)
            {
                if (names.Length > 0)
                    names.Append(", ");

                names.Append(hit.GetName());
            }

            if (hits.Count == 0)
                Console.WriteLine(String.Format("{0} attacked with Fire Breath and missed", GetName()));
            else
                Console.WriteLine(String.Format("{0} attacked with Fire Breath and did {1} damage to {2}", GetName(), damage, names.ToString()));

            foreach (var hit in hits)
                hit.Hit(damage);
        }

        public override void Hit(double damage)
        {
            damage = damage - 10;
            if (damage > 0)
                HealthPoints -= damage;

            if (HealthPoints > 0)
                Console.WriteLine(String.Format("{0} has {1} HP left", GetName(), HealthPoints));
            else
                Console.WriteLine(string.Format("{0} died", GetName()));
        }

        public override string GetName()
        {
            if (number > 0)
                return "Dragon " + number;
            else
                return "Dragon";
        }
    }
}
