﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameExampleAlt
{
    public class NullArmor : Armor
    {
        public override double GetDefense()
        {
            return 0;
        }
    }
}
