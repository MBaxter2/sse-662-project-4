﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSystem
{
    public class AcPowerSupply
    {
        private double voltage;
        private double current;
        private double frequency;
        private double phase;

        public AcPowerSupply()
        {
            voltage = 0;
            current = 1.0;
            frequency = 60;
            phase = 0;
        }

        public void SetVoltage(double voltage)
        {
            this.voltage = voltage;
        }

        public void SetCurrent(double current)
        {
            this.current = current;
        }

        public void SetFrequency(double frequency)
        {
            this.frequency = frequency;
        }

        public void SetPhase(double phase)
        {
            this.phase = phase;
        }

        public void On()
        {
            // Turn on power supply
        }

        public void Off()
        {
            // Turn off power supply
        }

        public void Reset()
        {
            Off();
            voltage = 0;
            current = 1.0;
            frequency = 60;
            phase = 0;
        }
    }
}
