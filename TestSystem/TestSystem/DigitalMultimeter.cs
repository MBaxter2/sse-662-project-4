﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSystem
{
    public enum DmmMeasurement{
        DcCurrent,
        DcVoltage,
        AcCurrent,
        AcVoltage,
        Resistance
    }
    
    public class DigitalMultimeter
    {
        private DmmMeasurement setup;
        private int simIndex;

        public DigitalMultimeter()
        {
            setup = DmmMeasurement.DcVoltage;
            simIndex = 0;
        }

        public void Reset()
        {
            setup = DmmMeasurement.DcVoltage;
        }

        public void Setup(DmmMeasurement setup)
        {
            this.setup = setup;
        }

        public double Measure()
        {
            return simMeasure();
        }

        private double simMeasure()
        {
            double[] measurements = { 5.0, 10.0, 2.0, 5.0, 0.2 };

            double result = measurements[simIndex];
            simIndex = simIndex >= 4 ? 0 : simIndex + 1;

            return result;
        }
    }
}
