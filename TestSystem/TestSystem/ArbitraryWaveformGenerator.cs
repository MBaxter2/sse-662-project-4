﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSystem
{
    public enum AwgMode
    {
        Sine,
        Ramp,
        Square,
        Triangle
    }

    public class ArbitraryWaveformGenerator
    {
        private double frequency;
        private double amplitude;
        private AwgMode mode;

        public ArbitraryWaveformGenerator()
        {
            frequency = 60;
            amplitude = 1.0;
            mode = AwgMode.Sine;
        }

        public void On()
        {
            // Turn on Awg output
        }

        public void Off()
        {
            // Turn off Awg output
        }

        public void SetFrequency(double frequency)
        {
            this.frequency = frequency;
        }

        public void SetAmplitude(double amplitude)
        {
            this.amplitude = amplitude;
        }

        public void SetMode(AwgMode mode)
        {
            this.mode = mode;
        }

        public void Reset()
        {
            frequency = 60;
            amplitude = 1.0;
            mode = AwgMode.Sine;
        }
    }
}
