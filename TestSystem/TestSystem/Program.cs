﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            TestSystem system = new TestSystem();

            Console.WriteLine("Begin Testing...");
            system.Reset();

            Test1(system);
            Test2(system);

            system.Reset();
            Console.WriteLine("System Reset");
            Console.ReadKey();
        }

        static void Test1(TestSystem system)
        {
            const double LOWER_TEST_LIMIT = 4.0;
            const double UPPER_TEST_LIMIT = 6.0;

            if (!system.PowerUp(5.0, 10.0))
                return;

            double response = system.MeasureFrequencyResponse(10);
            if (response <= UPPER_TEST_LIMIT && response >= LOWER_TEST_LIMIT)
                Console.WriteLine("Unit passed Test1");
            else
                Console.WriteLine("Unit failed Test1");

            system.PowerDown();
        }

        static void Test2(TestSystem system)
        {
            const double LOWER_TEST_LIMIT = 0.5;
            const double UPPER_TEST_LIMIT = 1.5;

            if(!system.PowerUp(2.0, 5.0))
                return;

            double current = system.MeasureCurrent();
            if (current <= UPPER_TEST_LIMIT && current >= LOWER_TEST_LIMIT)
                Console.WriteLine("Unit passed Test2");
            else
                Console.WriteLine("Unit failed Test2");

            system.PowerDown();
        }
    }
}
