﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSystem
{
    public class TestSystem
    {
        DcPowerSupply dcSupply;
        AcPowerSupply acSupply;
        DigitalMultimeter dmm;
        Oscilloscope scope;
        ArbitraryWaveformGenerator awg;

        public TestSystem()
        {
            dcSupply = new DcPowerSupply();
            acSupply = new AcPowerSupply();
            dmm = new DigitalMultimeter();
            scope = new Oscilloscope();
            awg = new ArbitraryWaveformGenerator();
        }

        public bool PowerUp(double dcVolt, double acVolt)
        {           
            dcSupply.SetVoltage(dcVolt);
            dcSupply.SetCurrent(2.0);
            dcSupply.On();

            dmm.Setup(DmmMeasurement.DcVoltage);
            double voltage = dmm.Measure();

            if (voltage > dcVolt + 1 || voltage < dcVolt - 1)
            {
                dcSupply.Off();
                return false;
            }

            acSupply.SetVoltage(acVolt);
            acSupply.SetCurrent(2.0);
            acSupply.On();

            dmm.Setup(DmmMeasurement.AcVoltage);
            voltage = dmm.Measure();

            if (voltage > acVolt + 1 || voltage < acVolt - 1)
            {
                dcSupply.Off();
                acSupply.Off();
                return false;
            }

            return true;
        }

        public bool PowerDown()
        {
            acSupply.Off();
            dcSupply.Off();

            return true;
        }

        public double MeasureFrequencyResponse(double frequency)
        {
            awg.SetFrequency(frequency);
            awg.SetAmplitude(5.0);
            awg.SetMode(AwgMode.Sine);

            scope.ChannelSetup(0, true, 2.0);
            scope.HorizontalSetup(1 / frequency * 4);
            scope.SetMeasurementMode(OscilloscopeMeasurements.Amplitude);
            scope.TriggerSetup(0, 2.2);

            return scope.Measure();
        }

        public double MeasureCurrent()
        {
            awg.SetFrequency(60);
            awg.SetAmplitude(5.0);
            awg.SetMode(AwgMode.Square);

            dmm.Setup(DmmMeasurement.AcCurrent);

            return dmm.Measure();
        }

        public void Reset()
        {
            dcSupply.Reset();
            acSupply.Reset();
            dmm.Reset();
            scope.Reset();
            awg.Reset();
        }
    }
}
