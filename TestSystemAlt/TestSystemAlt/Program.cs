﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSystemAlt
{
    class Program
    {
        static void Main(string[] args)
        {
            DcPowerSupply dcSupply = new DcPowerSupply();
            AcPowerSupply acSupply = new AcPowerSupply();
            DigitalMultimeter dmm = new DigitalMultimeter();
            Oscilloscope scope = new Oscilloscope();
            ArbitraryWaveformGenerator awg = new ArbitraryWaveformGenerator();

            Console.WriteLine("Begin Testing...");
            dcSupply.Reset();
            acSupply.Reset();
            dmm.Reset();
            scope.Reset();
            awg.Reset();

            Test1(dcSupply, acSupply, dmm, awg, scope);
            Test2(dcSupply, acSupply, dmm, awg);

            dcSupply.Reset();
            acSupply.Reset();
            dmm.Reset();
            scope.Reset();
            awg.Reset();
            Console.WriteLine("System Reset");
            Console.ReadKey();
        }

        static void Test1(DcPowerSupply dcSupply, AcPowerSupply acSupply, DigitalMultimeter dmm, ArbitraryWaveformGenerator awg, Oscilloscope scope)
        {
            const double LOWER_TEST_LIMIT = 4.0;
            const double UPPER_TEST_LIMIT = 6.0;

            dcSupply.SetVoltage(5.0);
            dcSupply.SetCurrent(2.0);
            dcSupply.On();

            dmm.Setup(DmmMeasurement.DcVoltage);
            double voltage = dmm.Measure();

            if (voltage > 5.0 + 1 || voltage < 5.0 - 1)
            {
                dcSupply.Off();
                return;
            }

            acSupply.SetVoltage(10.0);
            acSupply.SetCurrent(2.0);
            acSupply.On();

            dmm.Setup(DmmMeasurement.AcVoltage);
            voltage = dmm.Measure();

            if (voltage > 10.0 + 1 || voltage < 10.0 - 1)
            {
                dcSupply.Off();
                acSupply.Off();
                return;
            }

            awg.SetFrequency(10);
            awg.SetAmplitude(5.0);
            awg.SetMode(AwgMode.Sine);

            scope.ChannelSetup(0, true, 2.0);
            scope.HorizontalSetup(1 / 10.0 * 4);
            scope.SetMeasurementMode(OscilloscopeMeasurements.Amplitude);
            scope.TriggerSetup(0, 2.2);

            double response = scope.Measure();
            if (response <= UPPER_TEST_LIMIT && response >= LOWER_TEST_LIMIT)
                Console.WriteLine("Unit passed Test1");
            else
                Console.WriteLine("Unit failed Test1");

            acSupply.Off();
            dcSupply.Off();
        }

        static void Test2(DcPowerSupply dcSupply, AcPowerSupply acSupply, DigitalMultimeter dmm, ArbitraryWaveformGenerator awg)
        {
            const double LOWER_TEST_LIMIT = 0.5;
            const double UPPER_TEST_LIMIT = 1.5;
            dcSupply.SetVoltage(2.0);
            dcSupply.SetCurrent(2.0);
            dcSupply.On();

            dmm.Setup(DmmMeasurement.DcVoltage);
            double voltage = dmm.Measure();

            if (voltage > 2.0 + 1 || voltage < 2.0 - 1)
            {
                dcSupply.Off();
                return;
            }

            acSupply.SetVoltage(5.0);
            acSupply.SetCurrent(2.0);
            acSupply.On();

            dmm.Setup(DmmMeasurement.AcVoltage);
            voltage = dmm.Measure();

            if (voltage > 5.0 + 1 || voltage < 5.0 - 1)
            {
                dcSupply.Off();
                acSupply.Off();
                return;
            }

            awg.SetFrequency(60);
            awg.SetAmplitude(5.0);
            awg.SetMode(AwgMode.Square);

            dmm.Setup(DmmMeasurement.AcCurrent);

            double current = dmm.Measure();
            if (current <= UPPER_TEST_LIMIT && current >= LOWER_TEST_LIMIT)
                Console.WriteLine("Unit passed Test2");
            else
                Console.WriteLine("Unit failed Test2");

            acSupply.Off();
            dcSupply.Off();
        }
    }
}
