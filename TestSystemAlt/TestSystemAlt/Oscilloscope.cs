﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSystemAlt
{
    public enum OscilloscopeMeasurements
    {
        Amplitude,
        Frequency,
        Period,
        RiseTime,
        FallTime
    }
    
    public class Oscilloscope
    {
        private const int MAX_CHANNELS = 3;

        private struct channelSettings
        {
            public bool on;
            public double scale;
        }

        private channelSettings[] channels;
        private int triggerChannel;
        private double triggerLevel;
        private double horizontalScale;
        private OscilloscopeMeasurements mode;

        public Oscilloscope()
        {
            channels = new channelSettings[MAX_CHANNELS];
            triggerChannel = 0;
            triggerLevel = 0;
            horizontalScale = 10;
            mode = OscilloscopeMeasurements.Amplitude;
        }

        public void ChannelSetup(int channel, bool on, double verticalScale)
        {
            if(channel >= MAX_CHANNELS || channel < 0)
                throw new ArgumentOutOfRangeException("Invalid channel number");

            channels[channel].on = on;
            channels[channel].scale = verticalScale;
        }

        public void TriggerSetup(int channel, double level)
        {
            if (channel >= MAX_CHANNELS || channel < 0)
                throw new ArgumentOutOfRangeException("Invalid channel number");

            triggerChannel = channel;
            triggerLevel = level;
        }

        public void HorizontalSetup(double scale)
        {
            horizontalScale = scale;
        }

        public void SetMeasurementMode(OscilloscopeMeasurements mode)
        {
            this.mode = mode;
        }

        public double Measure()
        {
            return simMeasure();
        }

        public void Reset()
        {
            channels = new channelSettings[MAX_CHANNELS];
            triggerChannel = 0;
            triggerLevel = 0;
            horizontalScale = 10;
            mode = OscilloscopeMeasurements.Amplitude;
        }

        private double simMeasure()
        {
            return 5.0;
        }
    }
}
