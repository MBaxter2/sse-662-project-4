﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSystemAlt
{
    public class DcPowerSupply
    {
        private double voltage;
        private double current;

        public DcPowerSupply()
        {
            voltage = 0;
            current = 0;
        }

        public void SetVoltage(double voltage)
        {
            this.voltage = voltage;
        }

        public void SetCurrent(double current)
        {
            this.current = current;
        }

        public void On()
        {
            // Turn on power supply
        }

        public void Off()
        {
            // Turn off power supply
        }

        public void Reset()
        {
            Off();
            voltage = 0;
            current = 0;
        }
    }
}
